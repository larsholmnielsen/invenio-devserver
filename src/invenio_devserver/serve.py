import os
import time
import shutil
import signal
import optparse
import socket
import traceback
from itertools import chain

try:
    from functools import partial
except ImportError:
    def partial(fn, *initialargs, **initialkwds):
        def proxy(*finalargs, **finalkwds):
            args = initialargs + finalargs
            kwds = initialkwds.copy()
            kwds.update(finalkwds)
            return fn(*args, **kwds)
        return proxy


from invenio_devserver.webserver import run_simple
from werkzeug._internal import _log

from invenio_devserver import config

try:
    BaseException
except NameError:
    # Python 2.4 compatibility
    BaseException = Exception

DESCRIPTION = "Invenio web server for development"
USAGE_MESSAGE = "python serve.py [-bp]"


def get_extension(filename):
    try:
        return filename.rsplit('.', 1)[1]
    except IndexError:
        return ''


def valid_extension(filename, dirs=config.DIRS):
    """Checks the extension of the file to see if we should monitor it"""
    return get_extension(filename) in dirs.keys()


def generate_invenio_files_list(invenio_path=config.SRC_PATH):
    """Generates the list of all the source files to be monitored"""
    if isinstance(invenio_path, basestring):
        invenio_path = [invenio_path]

    def iter_files(dirname, filenames):
        return (os.path.join(dirname, filename) \
                      for filename in filenames if valid_extension(filename))

    def iter_folder(folder):
        return chain(*(
            iter_files(dirname, filenames) \
                                for dirname, _, filenames in os.walk(folder)
        ))

    return chain(*(iter_folder(folder) for folder in invenio_path))


def select_destination_path(filename, install_path=config.INSTALL_PATH, dests=config.DIRS):
    lib_dir = dests[get_extension(filename)]
    return os.path.join(install_path,
                        lib_dir,
                        os.path.basename(filename))


def reloader_loop(files, reloader=None, interval=1):
    """Monitor files, copies files to invenio install when they change and
    eventually restart our worker process"""
    mtimes = {}
    while 1:
        has_changes = False

        for filename in set(files):
            try:
                stats = os.stat(filename)
            except AttributeError:
                continue
            except OSError:
                continue

            mtime = stats.st_mtime
            old_time = mtimes.get(filename)
            if old_time is None:
                mtimes[filename] = mtime
                continue
            elif mtime > old_time:
                mtimes[filename] = mtime
                # Sleep for a while to wait for the texteditor to
                # finish writing the file
                time.sleep(0.1)
                dest = select_destination_path(filename)
                _log('info', ' * Detected change in %r, copying to %s' % \
                    (filename, dest))
                shutil.copyfile(filename, dest)
                has_changes = True

        if has_changes and reloader:
            reloader()
            time.sleep(1)

        time.sleep(interval)


class Reloader(object):
    """Function object that reloads the worker when called"""

    def __init__(self, options, server_socket):
        """Reloader initializer

        Saves:
        * worker pid to be able to kill it
        * server socket that should be given to the worker
                        to accept connections
        """
        self.options = options
        self.worker_pid = None
        self.server_socket = server_socket

    def __call__(self):
        """Called to reload the worker"""
        if self.worker_pid:
            kill_worker(self.worker_pid)
        try:
            self.worker_pid = spawn_server(self.options, self.server_socket)
        except:
            # Set to None in case, we kill the whole process group
            self.worker_pid = None
            raise


def start_server(options, server_socket, static_files=config.STATIC_FILES):
    """Start a new http server

    Called by the worker
    """
    # We only import wsgi here because this import some invenio components
    # and we do not want anything imported from invenio in the parent
    import wsgi

    wsgi.replace_error_handler()
    wsgi.wrap_warn()

    static = dict([(k, config.INSTALL_PATH + v) for k, v in static_files.items()])

    wsgi_app = partial(wsgi.application, options)

    if options.use_pdb:
        import pdb

        def pdb_on_error(f, *args, **kwargs):
            try:
                f(*args, **kwargs)
            except:
                pdb.post_mortem()

        wsgi_app = partial(pdb_on_error, wsgi_app)

    run_simple(server_socket,
               wsgi_app,
               use_debugger=True,
               use_evalex=True,
               static_files=static)


def spawn_server(options, server_socket):
    """Create a new worker"""
    _log('info', ' * Spawning worker')
    pid = os.fork()
    if pid == 0:
        start_server(options, server_socket)
    return pid


def parse_cli_options():
    """Parse command line options"""
    parser = optparse.OptionParser(description=DESCRIPTION,
                                   usage=USAGE_MESSAGE)
    # Display help and exit
    parser.add_option('-b', dest='bind_address', default='localhost',
                                                    help='Address to bind to')
    parser.add_option('-p', dest='bind_port', type='int', default=4000,
                                    help='Port to bind to')
    parser.add_option('--no-reload', action='store_false', dest='auto_reload',
                      default=True, help='Disable automatic reloading\n'\
                      'when a source file is changed')
    parser.add_option('--no-http', action='store_false', dest='serve_http',
                      default=True, help='Disable http server, only update ' \
                      'invenio install')
    parser.add_option('--buffer-output', dest='buffer_output', default=False,
                      action='store_true', help='Buffer output to display\n' \
                      'to display debug pages')
    parser.add_option('--pdb', dest='use_pdb', default=False,
                      action='store_true', help='Drop to python debugger\n' \
                      'on errors')
    parser.add_option('-s', action='append', dest='src_path', metavar='SRC_PATH',
                      default=[], help='Source folder (one or more)')
    parser.add_option('-o', dest='install_path', metavar='INSTALL_PATH',
                      default=[], help='Path to Invenio installation.')
    return parser.parse_args()


def kill_worker(pid):
    """Kill worker with <pid>"""
    _log('info', ' * Killing worker %r' % pid)
    os.kill(pid, signal.SIGTERM)
    _log('info', ' * Waiting for worker to stop')
    os.waitpid(pid, 0)


def create_socket(server_address, server_port):
    """Bind socket"""
    s = socket.socket(socket.AF_INET,
                           socket.SOCK_STREAM)
    s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    s.bind((server_address, server_port))
    return s


def bind_socket(options, ssl_context=None):
    server_address, server_port = options.bind_address, options.bind_port
    server_socket = create_socket(server_address, server_port)
    display_hostname = server_address != '*' and server_address or 'localhost'
    if ':' in display_hostname:
        display_hostname = '[%s]' % display_hostname
    _log('info', ' * Running on %s://%s:%d/', ssl_context is None
                         and 'http' or 'https', display_hostname, server_port)
    return server_socket


def prepare_server(options, ssl_context=None):
    """Prepare http server

    First binds the socket to accept connections from,
    then create a worker that will start a http server
    """
    server_socket = bind_socket(options, ssl_context)

    reloader = None
    # Create a new process group
    # Used for cleanup when quiting
    os.setpgrp()
    try:
        reloader = Reloader(options, server_socket)
        # Start first worker
        reloader()
        if reloader.worker_pid:
            invenio_files = list(generate_invenio_files_list())
            reloader_loop(invenio_files, reloader)
    except BaseException, e:
        if reloader and reloader.worker_pid:
            kill_worker(reloader.worker_pid)
        else:
            # We are killing ourselves
            # The python interpreter will not get a chance to print
            # the traceback
            if not isinstance(e, KeyboardInterrupt) \
                                            and not isinstance(e, SystemExit):
                print traceback.format_exc()[:-1]
            os.killpg(0, signal.SIGKILL)
        raise


def _main():
    """Script entrance"""
    (options, args) = parse_cli_options()

    # Override config SRC_PATH and INSTALL_PATH
    if options.src_path:
        config.SRC_PATH = [os.path.expanduser(x) for x in options.src_path]
    if options.install_path:
        config.INSTALL_PATH = os.path.expanduser(options.install_path)

    if options.serve_http and options.auto_reload:
        print 'HTTP Server mode with reload mode'
        prepare_server(options)
    elif options.serve_http:
        print 'Simple HTTP Server mode'
        start_server(options, bind_socket(options))
    elif options.auto_reload:
        print 'Copy-file only mode'
        invenio_files = list(generate_invenio_files_list())
        reloader_loop(invenio_files)


def main():
    try:
        _main()
    except KeyboardInterrupt:
        print 'Exiting'


if __name__ == '__main__':
    main()